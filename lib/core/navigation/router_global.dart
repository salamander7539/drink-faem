import 'package:faem_drink/Screens/HomeScreen/View/home_screen.dart';
import 'package:flutter/material.dart';
import '../../splash_screen.dart';

class RouterGlobal {
  RouterGlobal._();

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'home':
        return MaterialPageRoute(builder: (_) => HomeScreen());
      // case 'login':
      //   return MaterialPageRoute(builder: (_) => LoginPage());
      // case 'support':
      //   return MaterialPageRoute(builder: (_) => SupportPage());
      // case 'info':
      //   return MaterialPageRoute(builder: (_) => InfoPage());
      // case 'home':
      //   return MaterialPageRoute(builder: (_) => HomePage());
      // case 'orderPage':
      //   return MaterialPageRoute(builder: (_) => TaxiPage());
      case 'SplashScreen':
        return MaterialPageRoute(builder: (_) => SplashScreen());


      default:
        return MaterialPageRoute(
            builder: (_) => HomeScreen());
    }
  }
}
