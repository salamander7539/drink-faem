import 'package:flutter/material.dart';

class Keys {
  Keys._();
  static final authVerificationKey = GlobalKey<NavigatorState>();
  static final globalNavigationKey = GlobalKey<NavigatorState>();
}