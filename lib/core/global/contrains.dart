//--- for test ---


const String JOKE_ENDPOINT = 'https://api.chucknorris.io/jokes/random';

//---  Messages  ---

const String CHECK_NETWORK = 'Check network connection';

//---  Api  ---

const String authApiUrl = 'https://auth.apis.stage.faem.pro/api/v3';
const String apiUrl = 'https://eda-clients.apis.stage.faem.pro/api/v3';
const String centrifugoClientApi = 'wss://centrifugo.stage.faem.pro/connection/websocket?format=protobuf';
const String centrifugoTokenApi = 'https://notifier.apis.stage.faem.pro/api/v2/connectiontoken';

const String BASE_URL = 'https://crm.apis.stage.faem.pro/api/v2';
const String CRM_BASE_URL = 'https://crm.apis.stage.faem.pro/api/v2';
// https://crm.apis.stage.faem.pro/api/v2/orders/tariffs



const String AUTHENTICATION_ENDPOINT = '$authApiUrl/clients/new';
const String VERIFICATION_ENDPOINT = '$authApiUrl/clients/verification';
// const String BUILD_ROUTE_WAY_ENDPOINT = '$CLIENT_BASE_URL/buildrouteway';
const String REFRESH_JWT_ENDPOINT = '$authApiUrl/auth/clients/refresh';
const String CANCEL_ORDER = '$apiUrl/orders/cancel';


const String AUTOCOMPLETE_ENDPOINT = '$BASE_URL/addresses';
const String TARIFFS_ENDPOINT = '$BASE_URL/orders/tariffs';

const String CLOSEST_REGION_ENDPOINT = '$CRM_BASE_URL/clients/whereami';
const String SOURCE_UUID_ENDPOINT = '$CRM_BASE_URL/clients/get_source_uuid';
const String ORDER_ENDPOINT = '$CRM_BASE_URL/orders';


const String CENTRIFUGO_ENDPOINT = " https://notifier.apis.stage.faem.pro/api/v2/connectiontoken";



// "https://client.apis.stage.faem.pro/api/v2/auth/verification"

// const String REGISTRATION_ENDPOINT = '$BASE_URL/api';



// ---  HTTP status code  ---
//TODO: customize codes
const Map<int, String> HTTP_STATUS_CODE = {
  100: 'Continue',
  101: 'Switching Protocols',
  102: 'Processing',
  103: 'Early Hints',
  200: 'OK',
  201: 'Created',
  202: 'Accepted',
  203: 'Non-Authoritative Information',
  204: 'No Content',
  205: 'Reset Content',
  206: 'Partial Content',
  207: 'Multi-Status',
  208: 'Already Reported',
  226: 'IM Used',
  300: 'Multiple Choices',
  301: 'Moved Permanently',
  302: 'Moved Temporarily',
  303: 'See Other',
  304: 'Not Modified',
  305: 'Use Proxy',
  306: 'Reserved',
  307: 'Temporary Redirect',
  308: 'Permanent Redirect',
  400: 'Bad Request',
  401: 'Wrong email or password', //'Unauthorized',
  402: 'Payment Required',
  403: 'Forbidden',
  404: 'Not Found',
  405: 'Method Not Allowed',
  406: 'Not Acceptable',
  407: 'Proxy Authentication Required',
  408: 'Request Timeout',
  409: 'Conflict',
  410: 'Gone',
  411: 'Length Required',
  412: 'Precondition Failed',
  413: 'Payload Too Large',
  414: 'URI Too Long',
  415: 'Unsupported Media Type',
  416: 'Range Not Satisfiable',
  417: 'Expectation Failed',
  418: 'I’m a teapot',
  419: 'Authentication Timeout',
  421: 'Misdirected Request',
  422: 'Unprocessable Entity',
  423: 'Locked',
  424: 'Failed Dependency',
  425: 'Too Early',
  426: 'Upgrade Required',
  428: 'Precondition Required',
  429: 'Too Many Requests',
  431: 'Request Header Fields Too Large',
  449: 'Retry With',
  451: 'Unavailable For Legal Reasons',
  499: 'Client Closed Request',
  500: 'Internal Server Error',
  501: 'Not Implemented',
  502: 'Bad Gateway',
  503: 'Service Unavailable',
  504: 'Gateway Timeout',
  505: 'HTTP Version Not Supported',
  506: 'Variant Also Negotiates',
  507: 'Insufficient Storage',
  508: 'Loop Detected',
  509: 'Bandwidth Limit Exceeded',
  510: 'Not Extended',
  511: 'Network Authentication Required',
  520: 'Unknown Error',
  521: 'Web Server Is Down',
  522: 'Connection Timed Out',
  523: 'Origin Is Unreachable',
  524: 'A Timeout Occurred',
  525: 'SSL Handshake Failed',
  526: 'Invalid SSL Certificate',
};
