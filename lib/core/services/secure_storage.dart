import 'package:flutter/foundation.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

abstract class SecureStorage {
  Future init();
  Future<String> getData(String key);
  Future putData(String key, String value);
  Future removeData(String key);
  Future close();
}

class SecureStorageImpl implements SecureStorage {
  final FlutterSecureStorage storage;

  SecureStorageImpl({@required this.storage}) : assert(storage != null);

  @override
  Future init() async {
    print("init secureStorage");
  }

  @override
  // ignore: missing_return
  Future<String> getData(String key) async {
    return await storage.read(key: key);
  }

  @override
  Future putData(String key, String value) async {
    await storage.write(key: key, value: value);
  }

  @override
  Future removeData(String key) async {
    await storage.delete(key: key);
  }

  @override
  Future close() async {}
}