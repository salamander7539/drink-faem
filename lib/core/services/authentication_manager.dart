
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:faem_drink/core/models/main_user.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:flutter/foundation.dart';

class AuthenticationManager extends Cubit<bool> {
  final SecureStorage secureStorage;

  AuthenticationManager({@required this.secureStorage}) : assert(secureStorage != null), super(false);

  Future init() async {
    final rawUser = await secureStorage.getData('MainUser');

    if(rawUser != null) {
      final MainUser mainUser = MainUser.fromJson(json.decode(rawUser));
      print('TOKEN __ ${mainUser.verificationData.token}');
      if (mainUser?.customer != null) emit(true);
    }
  }

  Future <List> getUserNameAndPhone() async {
    String name;
    String phone;
    String token;
    final rawUser = await secureStorage.getData('MainUser');
    print("RAW ${rawUser.toString()}");
    if (rawUser != null) {
      final MainUser mainUser = MainUser.fromJson(json.decode(rawUser));
      name = mainUser.customer.firstname;
      phone = mainUser.customer.phone;
      token = mainUser.verificationData.token;
    }
    print('$name, $phone, $token');
    return [name, phone, token];
  }

  void activate() => emit(true);

  void deactivate() => emit(false);

  bool get isAuth => state;
}