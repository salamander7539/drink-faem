import 'dart:convert';
import 'package:faem_drink/core/global/contrains.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_messaging/firebase_messaging.dart';

class FirebaseService {
  FirebaseMessaging _firebaseMessaging;

  Future setupFirebase() async {
    _firebaseMessaging = FirebaseMessaging();
  }

  Future getFirebaseToken() async {
    return await _firebaseMessaging.getToken();
  }

  Future<bool> sendFCMToken({@required String fcmToken, @required String bearerToken, @required String deviceId}) async {
    print('sendFCMToken____');

    var headers = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json',
      'Source':'ios_client_app_1',
      'Authorization':'Bearer ' + bearerToken
    };

    var jsonRequest = jsonEncode({
      "token": fcmToken,
      'client_device_id': deviceId
    });

    var request = http.Request('POST', Uri.parse('$apiUrl/firebasetoken'));

    request.headers.addAll(headers);
    request.body = jsonRequest;

    http.StreamedResponse response = await request.send();

    print("sendFCMToken____response.statusCode_____${response.statusCode}");

    if (response.statusCode == 200 ) {
      print(await response.stream.bytesToString());
      return true;
    } else {
      print(response.reasonPhrase);
      return false;
    }
  }
}