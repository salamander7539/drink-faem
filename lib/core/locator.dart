import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:faem_drink/core/services/firebase.dart';
import 'package:faem_drink/core/services/network_info.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
// import 'package:geolocator/geolocator.dart';


final locator = GetIt.instance;

void locatorSetup(){
  locator.registerLazySingleton<FirebaseService>(() => FirebaseService());
  // locator.registerLazySingleton<Geolocator>(() => Geolocator());
  locator.registerLazySingleton<FlutterSecureStorage>(() => FlutterSecureStorage());

  locator.registerSingleton<SecureStorage>(SecureStorageImpl(storage: locator.get<FlutterSecureStorage>()));
  locator.registerLazySingleton<DataConnectionChecker>(() => DataConnectionChecker());
  locator.registerLazySingleton<NetworkInfo>(() => NetworkInfo(connectionChecker: locator.get<DataConnectionChecker>()));
  locator.registerSingleton<AuthenticationManager>(AuthenticationManager(secureStorage: locator.get<SecureStorage>()));
}