import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:flutter/foundation.dart';


@immutable
class MainUser {
  final VerificationData verificationData;
  final Customer customer;
  final String fcmToken;
  final String centrifugotoken;


  MainUser({
    this.verificationData,
    this.customer,
    this.fcmToken,
    this.centrifugotoken,
  });

  factory MainUser.fromJson(Map<String, dynamic> json) => MainUser(
    verificationData: VerificationData.fromJson(json["verificationData"]),
    customer: Customer.fromJson(json["customer"]),
    fcmToken: json["fcmToken"] ,
    centrifugotoken: json["centrifugotoken"] ,
  );

  Map<String, dynamic> toJson() => {
    "verificationData": verificationData,
    "customer": customer,
    "fcmToken": fcmToken,
    "centrifugotoken": centrifugotoken,
  };


  MainUser copyWith({
    VerificationData verificationData,
    Customer customer,
    String fcmToken,
    String centrifugotoken,
  }) {
    return MainUser(
      verificationData: verificationData ?? this.verificationData,
      customer: customer ?? this.customer,
      fcmToken: fcmToken ?? this.fcmToken,
      centrifugotoken: centrifugotoken ?? this.centrifugotoken,
    );
  }


}

@immutable
class Customer {
  final String firstname;
  final String lastname;
  final String phone;



  Customer({
    this.firstname,
    this.lastname,
    this.phone,
  });


  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
    firstname: json["firstname"],
    lastname: json["lastname"],
    phone: json["phone"],
  );

  Map<String, dynamic> toJson() => {
    "firstname": firstname,
    "lastname": lastname,
    "phone": phone,
  };
}
