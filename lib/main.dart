import 'package:faem_drink/app.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'core/locator.dart';
import 'core/services/firebase.dart';
import 'core/services/secure_storage.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white, // Color for Android
      statusBarBrightness: Brightness.light // Dark == white status bar -- for IOS.
  ));


    WidgetsFlutterBinding.ensureInitialized();
    locatorSetup();
    await Firebase.initializeApp();
    // await   FirebaseNotifications().setUpFirebase();
    await locator.get<FirebaseService>().setupFirebase();
    await locator.get<SecureStorage>().init();

    runApp(App());
}
