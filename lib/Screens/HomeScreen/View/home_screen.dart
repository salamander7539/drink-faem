import 'package:device_info/device_info.dart';
import 'package:faem_drink/Config/config.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Bloc/auth_phone_bloc.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Injections/auth_phone_repository_di.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/View/auth_phone_screen.dart';
import 'package:faem_drink/Screens/HistoryScreen/View/history_screen.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:faem_drink/data/global_variables.dart';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:io';

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  int pageIndex = 0;
  TabController tabController;
  final AuthenticationManager authenticationManager = locator.get<AuthenticationManager>();

  Widget _buildContainer({int index, String categoryName}) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverToBoxAdapter(
          child: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: Container(
              child: Column(
                children: [
                  Text(
                    categoryName,
                    style: TextStyle(fontSize: 24),
                  ),
                ],
              ),
            ),
          ),
        ),
        SliverGrid(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  color: Colors.transparent,
                  elevation: 0.0,
                  // ignore: deprecated_member_use
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                    ),
                    color: Colors.transparent,
                    onPressed: () {},
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          color: Colors.black,
                          height: MediaQuery.of(context).size.height * 0.2,
                        ),
                        Text(
                          'Чёрный айс-ти',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          '250 ₽',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.blue,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
            childCount: 25,
          ),
          gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 200.0,
            mainAxisSpacing: 20.0,
            crossAxisSpacing: 20.0,
            childAspectRatio: .8,
          ),
        ),
      ],
    );
  }

  Widget _categoryList({int index, String categoryName}) {
    // ignore: deprecated_member_use
    return Tab(
      text: categoryName,
    );
  }

  List useData;


  _setUserName() async {
    List data;
    if(authenticationManager.isAuth){
      data =  await authenticationManager.getUserNameAndPhone();

    }

    setState(() {
      useData = data;
      print(useData.toString());
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _setUserName();
    tabController = TabController(length: 4, vsync: this, initialIndex: 0);
    // devId = NecessaryDataForAuth.getData();
    NecessaryDataForAuth.getData();
    id = NecessaryDataForAuth().device_id;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("authenticationManager_____ ${authenticationManager.isAuth}");

    return Scaffold(
        backgroundColor: Colors.white,
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.white,
                shadowColor: Colors.transparent,
                expandedHeight: 200,
                pinned: true,
                floating: true,
                snap: true,
                bottom: PreferredSize(
                  // Add this code
                  preferredSize: Size.fromHeight(56), // Add this code
                  child: ConstrainedBox(
                    constraints: new BoxConstraints(
                      maxHeight: 50.0,
                      minWidth: MediaQuery.of(context).size.width,
                    ),
                    child: Container(
                      child: TabBar(
                        isScrollable: true,
                        labelPadding: EdgeInsets.symmetric(horizontal: 20.0),
                        labelStyle: TextStyle(fontSize: 18),
                        labelColor: Colors.black,
                        controller: tabController,
                        tabs: <Widget>[
                          _categoryList(index: 0, categoryName: 'Новинки'),
                          _categoryList(index: 1, categoryName: 'Горячие'),
                          _categoryList(index: 2, categoryName: 'Еда'),
                          _categoryList(index: 3, categoryName: 'Холодное'),
                        ],
                      ),
                    ),
                  ), // Add this code
                ),
                title: Stack(
                  // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 15.0, left: 15),
                      child: Text(
                        'FaemDrink',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: Container(
                        // ignore: deprecated_member_use
                        child: FlatButton.icon(
                            onPressed: () {},
                            icon: Icon(
                              Icons.location_on,
                              color: Colors.blue,
                            ),
                            label: Text('Нальчик')),
                      ),
                    ),
                  ],
                ),
                centerTitle: true,
                actions: [
                  InkWell(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: SvgPicture.asset(
                        'assets/svg_images/profile.svg',
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    onTap: () {
                      print("AUTH ${authenticationManager.isAuth}");
                      if (authenticationManager == null || !authenticationManager.isAuth) {
                        Navigator.push(
                          context,
                          new MaterialPageRoute(
                            builder: (context) => BlocProvider<AuthPhoneBlocImpl>(
                              create: (context) => AuthPhoneBlocImpl(
                                  repository: AuthPhoneRepositoryInject.authPhoneRepository(),),
                              child: AuthPhoneScreen(),
                            ),
                          ),
                        );
                      } else {
                        Navigator.push(
                          context,
                          new MaterialPageRoute(builder: (_) => HistoryScreen()),
                        );
                      }
                    },
                  ),
                ],
                flexibleSpace: Stack(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 30.0),
                    ),
                  ],
                ),
              ),
            ];
          },
          body: TabBarView(
            controller: tabController,
            children: [
              _buildContainer(categoryName: 'Новинки'),
              _buildContainer(categoryName: 'Горячее'),
              _buildContainer(categoryName: 'Еда'),
              _buildContainer(categoryName: 'Холодное'),
            ],
          ),
        )
        // CustomScrollView(
        //   slivers: <Widget>[
        //     SliverAppBar(
        //       backgroundColor: Colors.white,
        //       shadowColor: Colors.transparent,
        //       expandedHeight: 200,
        //       pinned: true,
        //       floating: true,
        //       snap: true,
        //       bottom: PreferredSize(
        //         // Add this code
        //         preferredSize: Size.fromHeight(56), // Add this code
        //         child: ConstrainedBox(
        //           constraints: new BoxConstraints(
        //             maxHeight: 50.0,
        //             minWidth: MediaQuery.of(context).size.width,
        //           ),
        //           child: Container(
        //             child: TabBar(
        //               isScrollable: true,
        //               labelPadding: EdgeInsets.symmetric(horizontal: 20.0),
        //               labelStyle: TextStyle(fontSize: 18),
        //               labelColor: Colors.black,
        //               controller: tabController,
        //               tabs: <Widget>[
        //                 _categoryList(index: 0, categoryName: 'Новинки'),
        //                 _categoryList(index: 1, categoryName: 'Горячие'),
        //                 _categoryList(index: 2, categoryName: 'Еда'),
        //                 _categoryList(index: 3, categoryName: 'Холодное'),
        //               ],
        //             ),
        //           ),
        //         ), // Add this code
        //       ),
        //       title: Stack(
        //         // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //         children: [
        //           Padding(
        //             padding: const EdgeInsets.only(top: 15.0, left: 15),
        //             child: Text(
        //               'FaemDrink',
        //               style: TextStyle(
        //                   fontSize: 18,
        //                   fontWeight: FontWeight.bold,
        //                   color: Colors.black),
        //               textAlign: TextAlign.center,
        //             ),
        //           ),
        //           Padding(
        //             padding: const EdgeInsets.only(top: 25.0),
        //             child: Container(
        //               // ignore: deprecated_member_use
        //               child: FlatButton.icon(
        //                   onPressed: () {},
        //                   icon: Icon(
        //                     Icons.location_on,
        //                     color: Colors.blue,
        //                   ),
        //                   label: Text('Нальчик')),
        //             ),
        //           ),
        //         ],
        //       ),
        //       centerTitle: true,
        //       actions: [
        //         InkWell(
        //           child: Padding(
        //             padding: const EdgeInsets.only(top: 8.0),
        //             child: SvgPicture.asset(
        //               'assets/svg_images/profile.svg',
        //               fit: BoxFit.fitHeight,
        //             ),
        //           ),
        //           onTap: () {
        //             Get.to(() => AuthPhoneScreen());
        //           },
        //         ),
        //       ],
        //       flexibleSpace: Stack(
        //         children: <Widget>[
        //           Padding(
        //             padding: const EdgeInsets.only(top: 30.0),
        //           ),
        //         ],
        //       ),
        //     ),
        //     // SliverToBoxAdapter(
        //     //   child: Container(
        //     //     height: MediaQuery.of(context).size.height,
        //     //     child: StaggeredGridView.countBuilder(
        //     //       physics: BouncingScrollPhysics(),
        //     //       padding: EdgeInsets.zero,
        //     //       crossAxisCount: 2,
        //     //       itemCount: 25,
        //     //       itemBuilder: (BuildContext context, int index) {
        //     //         return Padding(
        //     //           padding: const EdgeInsets.all(8.0),
        //     //           child: Card(
        //     //             color: Colors.transparent,
        //     //             elevation: 0.0,
        //     //             child: FlatButton(
        //     //               shape: RoundedRectangleBorder(
        //     //                 borderRadius: BorderRadius.circular(12),
        //     //               ),
        //     //               color: Colors.transparent,
        //     //               onPressed: () {},
        //     //               child: Column(
        //     //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        //     //                 crossAxisAlignment: CrossAxisAlignment.center,
        //     //                 children: [
        //     //                   Container(
        //     //                     color: Colors.black,
        //     //                     height: MediaQuery.of(context).size.height * 0.2,
        //     //                   ),
        //     //                   Text(
        //     //                     'Чёрный айс-ти',
        //     //                     style: TextStyle(
        //     //                       fontSize: 16,
        //     //                       fontWeight: FontWeight.bold,
        //     //                     ),
        //     //                   ),
        //     //                   Text(
        //     //                     '250 ₽',
        //     //                     style: TextStyle(
        //     //                       fontSize: 16,
        //     //                       color: Colors.blue,
        //     //                     ),
        //     //                   ),
        //     //                 ],
        //     //               ),
        //     //             ),
        //     //           ),
        //     //         );
        //     //       },
        //     //       staggeredTileBuilder: (int index) {
        //     //         return StaggeredTile.extent(1, 245);
        //     //       },
        //     //     ),
        //     //   ),
        //     // ),
        //     SliverToBoxAdapter(
        //       child: Container(
        //         height: 10,
        //       ),
        //     ),
        //     // _buildContainer(),
        //     SliverToBoxAdapter(
        //       child: SizedBox(
        //         height: 800,
        //         child: TabBarView(
        //           controller: tabController,
        //           children: <Widget>[
        //             _buildContainer(index: 0, pageColor: Colors.indigo),
        //             _buildContainer(index: 1, pageColor: Colors.red),
        //             _buildContainer(index: 2, pageColor: Colors.indigo),
        //             _buildContainer(index: 3, pageColor: Colors.red),
        //           ],
        //         ),
        //       ),
        //     ),
        //     // SliverToBoxAdapter(
        //     //   child: Padding(
        //     //     padding: const EdgeInsets.only(top: 18.0),
        //     //     child: Container(
        //     //       child: Text(
        //     //         'Name',
        //     //         style: TextStyle(fontSize: 24),
        //     //         textAlign: TextAlign.center,
        //     //       ),
        //     //     ),
        //     //   ),
        //     // ),
        //
        //
        //   ],
        // ),
        );
  }
}
