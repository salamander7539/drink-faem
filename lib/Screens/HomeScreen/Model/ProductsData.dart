// To parse this JSON data, do
//
//     final productsData = productsDataFromJson(jsonString);

import 'dart:convert';

List<ProductsData> productsDataFromJson(String str) => List<ProductsData>.from(json.decode(str).map((x) => ProductsData.fromJson(x)));

String productsDataToJson(List<ProductsData> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProductsData {
  ProductsData({
    this.uuid,
    this.externalId,
    this.name,
    this.storeUuid,
    this.barcodes,
    this.productCategories,
    this.comment,
    this.url,
    this.deleted,
    this.available,
    this.stopList,
    this.defaultSet,
    this.priority,
    this.type,
    this.price,
    this.meta,
    this.variantGroups,
  });

  String uuid;
  String externalId;
  String name;
  String storeUuid;
  dynamic barcodes;
  List<ProductCategory> productCategories;
  String comment;
  String url;
  bool deleted;
  bool available;
  bool stopList;
  bool defaultSet;
  int priority;
  Type type;
  int price;
  ProductsDatumMeta meta;
  dynamic variantGroups;

  factory ProductsData.fromJson(Map<String, dynamic> json) => ProductsData(
    uuid: json["uuid"] == null ? null : json["uuid"],
    externalId: json["external_id"] == null ? null : json["external_id"],
    name: json["name"] == null ? null : json["name"],
    storeUuid: json["store_uuid"] == null ? null : json["store_uuid"],
    barcodes: json["barcodes"],
    productCategories: json["product_categories"] == null ? null : List<ProductCategory>.from(json["product_categories"].map((x) => ProductCategory.fromJson(x))),
    comment: json["comment"] == null ? null : json["comment"],
    url: json["url"] == null ? null : json["url"],
    deleted: json["deleted"] == null ? null : json["deleted"],
    available: json["available"] == null ? null : json["available"],
    stopList: json["stop_list"] == null ? null : json["stop_list"],
    defaultSet: json["default_set"] == null ? null : json["default_set"],
    priority: json["priority"] == null ? null : json["priority"],
    type: json["type"] == null ? null : typeValues.map[json["type"]],
    price: json["price"] == null ? null : json["price"],
    meta: json["meta"] == null ? null : ProductsDatumMeta.fromJson(json["meta"]),
    variantGroups: json["variant_groups"],
  );

  Map<String, dynamic> toJson() => {
    "uuid": uuid == null ? null : uuid,
    "external_id": externalId == null ? null : externalId,
    "name": name == null ? null : name,
    "store_uuid": storeUuid == null ? null : storeUuid,
    "barcodes": barcodes,
    "product_categories": productCategories == null ? null : List<dynamic>.from(productCategories.map((x) => x.toJson())),
    "comment": comment == null ? null : comment,
    "url": url == null ? null : url,
    "deleted": deleted == null ? null : deleted,
    "available": available == null ? null : available,
    "stop_list": stopList == null ? null : stopList,
    "default_set": defaultSet == null ? null : defaultSet,
    "priority": priority == null ? null : priority,
    "type": type == null ? null : typeValues.reverse[type],
    "price": price == null ? null : price,
    "meta": meta == null ? null : meta.toJson(),
    "variant_groups": variantGroups,
  };
}

class ProductsDatumMeta {
  ProductsDatumMeta({
    this.shortDescription,
    this.description,
    this.composition,
    this.weight,
    this.weightMeasurement,
    this.oldPrice,
    this.images,
    this.energyValue,
  });

  String shortDescription;
  String description;
  String composition;
  double weight;
  WeightMeasurement weightMeasurement;
  int oldPrice;
  List<String> images;
  EnergyValue energyValue;

  factory ProductsDatumMeta.fromJson(Map<String, dynamic> json) => ProductsDatumMeta(
    shortDescription: json["short_description"] == null ? null : json["short_description"],
    description: json["description"] == null ? null : json["description"],
    composition: json["composition"] == null ? null : json["composition"],
    weight: json["weight"] == null ? null : json["weight"].toDouble(),
    weightMeasurement: json["weight_measurement"] == null ? null : weightMeasurementValues.map[json["weight_measurement"]],
    oldPrice: json["old_price"] == null ? null : json["old_price"],
    images: json["images"] == null ? null : List<String>.from(json["images"].map((x) => x)),
    energyValue: json["energy_value"] == null ? null : EnergyValue.fromJson(json["energy_value"]),
  );

  Map<String, dynamic> toJson() => {
    "short_description": shortDescription == null ? null : shortDescription,
    "description": description == null ? null : description,
    "composition": composition == null ? null : composition,
    "weight": weight == null ? null : weight,
    "weight_measurement": weightMeasurement == null ? null : weightMeasurementValues.reverse[weightMeasurement],
    "old_price": oldPrice == null ? null : oldPrice,
    "images": images == null ? null : List<dynamic>.from(images.map((x) => x)),
    "energy_value": energyValue == null ? null : energyValue.toJson(),
  };
}

class EnergyValue {
  EnergyValue({
    this.protein,
    this.fat,
    this.carbohydrates,
    this.calories,
  });

  int protein;
  int fat;
  int carbohydrates;
  int calories;

  factory EnergyValue.fromJson(Map<String, dynamic> json) => EnergyValue(
    protein: json["protein"] == null ? null : json["protein"],
    fat: json["fat"] == null ? null : json["fat"],
    carbohydrates: json["carbohydrates"] == null ? null : json["carbohydrates"],
    calories: json["calories"] == null ? null : json["calories"],
  );

  Map<String, dynamic> toJson() => {
    "protein": protein == null ? null : protein,
    "fat": fat == null ? null : fat,
    "carbohydrates": carbohydrates == null ? null : carbohydrates,
    "calories": calories == null ? null : calories,
  };
}

enum WeightMeasurement { EMPTY, WEIGHT_MEASUREMENT }

final weightMeasurementValues = EnumValues({
  "шт": WeightMeasurement.EMPTY,
  "": WeightMeasurement.WEIGHT_MEASUREMENT
});

class ProductCategory {
  ProductCategory({
    this.uuid,
    this.name,
    this.priority,
    this.comment,
    this.url,
    this.meta,
  });

  String uuid;
  String name;
  int priority;
  String comment;
  String url;
  ProductCategoryMeta meta;

  factory ProductCategory.fromJson(Map<String, dynamic> json) => ProductCategory(
    uuid: json["uuid"] == null ? null : json["uuid"],
    name: json["name"] == null ? null : json["name"],
    priority: json["priority"] == null ? null : json["priority"],
    comment: json["comment"] == null ? null : json["comment"],
    url: json["url"] == null ? null : json["url"],
    meta: json["meta"] == null ? null : ProductCategoryMeta.fromJson(json["meta"]),
  );

  Map<String, dynamic> toJson() => {
    "uuid": uuid == null ? null : uuid,
    "name": name == null ? null : name,
    "priority": priority == null ? null : priority,
    "comment": comment == null ? null : comment,
    "url": url == null ? null : url,
    "meta": meta == null ? null : meta.toJson(),
  };
}

class ProductCategoryMeta {
  ProductCategoryMeta();

  factory ProductCategoryMeta.fromJson(Map<String, dynamic> json) => ProductCategoryMeta(
  );

  Map<String, dynamic> toJson() => {
  };
}

enum Type { SINGLE }

final typeValues = EnumValues({
  "single": Type.SINGLE
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
