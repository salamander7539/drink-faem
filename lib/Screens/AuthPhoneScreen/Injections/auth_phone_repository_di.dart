
import 'auth_phone_api_provider_di.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/repositories/auth_phone_repository.dart';

class AuthPhoneRepositoryInject {
  static AuthPhoneRepository _authPhoneRepository;

  AuthPhoneRepositoryInject._();

  static AuthPhoneRepository authPhoneRepository() {
    if(_authPhoneRepository == null) {
      _authPhoneRepository = AuthPhoneRepositoryImpl(
        apiProvider: AuthPhoneApiProviderInject.authPhoneApiProvider(),
      );
    }
    return _authPhoneRepository;
  }
}