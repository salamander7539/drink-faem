
import 'package:faem_drink/Screens/AuthPhoneScreen/Provider/auth_phone_api_provider.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/repositories/auth_phone_repository.dart';

class AuthPhoneApiProviderInject {
  static AuthPhoneApiProvider _authPhoneApiProvider;

  AuthPhoneApiProviderInject._();

  static AuthPhoneApiProvider authPhoneApiProvider() {
    if(_authPhoneApiProvider == null) {
      _authPhoneApiProvider = AuthPhoneApiProviderImpl();
    }
    return _authPhoneApiProvider;
  }
}