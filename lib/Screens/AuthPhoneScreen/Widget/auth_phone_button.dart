import 'package:device_info/device_info.dart';
import 'package:faem_drink/Config/config.dart';
import 'package:faem_drink/Internet/check_internet.dart';
import 'package:faem_drink/Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Bloc/auth_phone_bloc.dart';
import 'package:faem_drink/Screens/VerificationScreen/View/verification_screen.dart';
import 'package:faem_drink/Screens/VerificationScreen/View/verification_screen_view.dart';
import 'package:faem_drink/core/global/keys.dart';
import 'package:faem_drink/data/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

class AuthPhoneButton extends StatefulWidget {
  AuthPhoneBlocImpl authPhoneBlocImpl;

  AuthPhoneButton({Key key, this.authPhoneBlocImpl}) : super(key: key);

  @override
  AuthPhoneButtonState createState() => new AuthPhoneButtonState(authPhoneBlocImpl);
}

class AuthPhoneButtonState extends State<AuthPhoneButton> {

  AuthPhoneBlocImpl authPhoneBlocImpl;

  AuthPhoneButtonState(this.authPhoneBlocImpl,);
  var devId;


  String validateMobile(String value) {
    String pattern = r'(^(?:[+]?7)[0-9]{10}$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return 'Укажите номер';
    } else if (!regExp.hasMatch(value)) {
      return 'Указан неверный номер';
    }
    return null;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getId();
  }

  void getId() async {
    devId = await DeviceInfoPlugin().androidInfo.then((value) => value.id);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: double.infinity,
        height: 44,
        decoration: BoxDecoration(
          color: Color(0xFFF2F6F9),
          borderRadius: BorderRadius.circular(32),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'получить код',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      onTap: () async {
        if(await Internet.checkConnection()) {
          if(authPhoneBlocImpl.state is VerificationLoadingState) {
            return;
          }
          currentUser.phone = currentUser.phone.replaceAll('-', '');
          currentUser.phone = currentUser.phone.replaceAll(' ', '');
          if(validateMobile(currentUser.phone) == null) {
            if (currentUser.phone[0] != '+') {
              currentUser.phone = '+' + currentUser.phone;
            }
            print(currentUser.phone);
            BlocProvider.of<AuthPhoneBlocImpl>(context).add(SendPhoneNumber(phoneNumber: currentUser.phone, deviceId: devId));
            Navigator.push(context, MaterialPageRoute(builder: (context) => VerificationScreen(phone: currentUser.phone,)));
          } else {
            BlocProvider.of<AuthPhoneBlocImpl>(context).add(SetError('Указан неверный номер'));
          }
        } else {
          noConnection(context);
        }
        // Navigator.push(context, MaterialPageRoute(builder: (context) => AuthCodeScreen()));
      },
    );
  }
}
