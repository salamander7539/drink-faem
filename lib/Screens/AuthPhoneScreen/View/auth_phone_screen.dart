import 'package:faem_drink/Screens/AuthPhoneScreen/Bloc/auth_phone_bloc.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Widget/auth_phone_button.dart';
import 'package:faem_drink/data/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class AuthPhoneScreen extends StatefulWidget {
  @override
  _AuthPhoneScreenState createState() => _AuthPhoneScreenState();
}

class _AuthPhoneScreenState extends State<AuthPhoneScreen> {
  var controller;
  Color colorButton;
  AuthPhoneBlocImpl authPhoneBlocImpl;
  GlobalKey<AuthPhoneButtonState> buttonStateKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    authPhoneBlocImpl = BlocProvider.of<AuthPhoneBlocImpl>(context);
    controller = new MaskedTextController(mask: '+7 000 000-00-00');
    buttonStateKey = new GlobalKey<AuthPhoneButtonState>();
    controller.afterChange = (String previous, String next) {
      if (next.length > previous.length) {
        controller.selection = TextSelection.fromPosition(
            TextPosition(offset: controller.text.length));
      }
      return false;
    };
    controller.beforeChange = (String previous, String next) {
      if (controller.text == '8') {
        controller.updateText('+7 ');
      }
      return true;
    };
    controller.text = '+7 ';
  }

  @override
  Widget build(BuildContext context) {
    authPhoneBlocImpl = BlocProvider.of<AuthPhoneBlocImpl>(context);
    return Scaffold(
      body: Stack(
        children: [
          Align(
            alignment: Alignment.topCenter,
            child: Padding(
              padding: const EdgeInsets.only(top: 50.0),
              child: Text(
                'Номер телефона',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xFFF2F3F5),
                  borderRadius: BorderRadius.circular(14),
                ),
                child: TextField(
                  autofocus: true,
                  controller: controller,
                  keyboardType: TextInputType.phone,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 25),
                  decoration: InputDecoration(
                    hintStyle: TextStyle(fontSize: 17),
                    border: InputBorder.none,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                  ),
                  onChanged: (value) async {
                    if (value == '+7 8') {
                      controller.text = '+7';
                    }
                    currentUser.phone = value;
                    print(currentUser.phone);
                  },
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: AuthPhoneButton(
                authPhoneBlocImpl: authPhoneBlocImpl,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
