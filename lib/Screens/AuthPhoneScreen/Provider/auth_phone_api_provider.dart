import 'package:dio/dio.dart';
import 'package:faem_drink/core/global/contrains.dart';
import 'package:faem_drink/data/global_variables.dart';

abstract class AuthPhoneApiProvider<T> {
  Future<T> authPhone(String deviceId, String phone);
}

class AuthPhoneApiProviderImpl implements AuthPhoneApiProvider {
  final Dio _dio = Dio();

  @override
  Future<String> authPhone (String deviceId, String phone) async {
    try {
      Response response = await _dio.post("https://auth.apis.stage.faem.pro/api/v3/clients/new", options: Options(headers: {
        'Source': header,
      }), data: {
        'device_id': deviceId,
        'phone': phone
      });
      if(response.statusCode==200){
        print(response.data);
        return response.data["message"];
      }else{
        print('Request failed with status: ${response.statusCode}.');
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }
}