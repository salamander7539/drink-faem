
import 'package:faem_drink/Internet/check_internet.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Provider/auth_phone_api_provider.dart';
import 'package:flutter/cupertino.dart';

abstract class AuthPhoneRepository<T> {
  Future<T> authPhone(String deviceId, String phone);
}

class AuthPhoneRepositoryImpl implements AuthPhoneRepository {
  final AuthPhoneApiProvider apiProvider;

  AuthPhoneRepositoryImpl({@required this.apiProvider}) : assert (apiProvider != null);

  static final RegExp phoneRegExp = RegExp(r"[^+0-9]+");

  @override
  Future authPhone(String deviceId, String phone) async {
    if(await Internet.checkConnection()) {
      final data = await apiProvider.authPhone(deviceId, phone.replaceAll(phoneRegExp, ''));
      return data;
    } else {
      print("Check network connection");
    }
  }
}