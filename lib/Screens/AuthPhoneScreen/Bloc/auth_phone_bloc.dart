import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Model/Auth.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/repositories/auth_phone_repository.dart';
import 'package:faem_drink/data/data.dart';
import 'package:faem_drink/data/global_variables.dart';
import 'package:faem_drink/data/refreshToken.dart';

part 'auth_phone_event.dart';

part 'auth_phone_state.dart';

abstract class AuthPhoneBloc extends Bloc<AuthPhoneEvent, AuthPhoneState> {
  AuthPhoneBloc(AuthPhoneState state) : super(state);
}

class AuthPhoneBlocImpl extends Bloc<AuthPhoneEvent, AuthPhoneState> {
  final AuthPhoneRepository repository;

  AuthPhoneBlocImpl({this.repository})
      : assert(repository != null),
        super(AuthPhoneInitialState());

  @override
  Stream<AuthPhoneState> mapEventToState(AuthPhoneEvent event) async* {
    if (event is SendPhoneNumber) {
      yield AuthPhoneLoadingState();
      try {
        final SMS = await repository.authPhone(
            event.deviceId, event.phoneNumber
          // "+79182246853"
          //'DF7243D2-AB8C-4609-AC60-D8815202A0FE'
        );
        print("phone__$SMS");
      } on Exception catch (error) {
        yield AuthPhoneErrorState(error.toString());
      }
    }
  }
}

