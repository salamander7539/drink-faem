part of 'auth_phone_bloc.dart';

abstract class AuthPhoneState extends Equatable {
  const AuthPhoneState();
  @override
  List<Object> get props => [];
}

class AuthPhoneInitialState extends AuthPhoneState {}

class AuthPhoneLoadingState extends AuthPhoneState {}

class AuthPhoneSuccessState extends AuthPhoneState {
  final AuthData authData;
  final bool goToHomeScreen;

  const AuthPhoneSuccessState(this.authData, {this.goToHomeScreen = false});

  @override
  List<Object> get props => [authData, goToHomeScreen];
}

class AuthPhoneErrorState extends AuthPhoneState {
  final String error;

  const AuthPhoneErrorState(this.error);

  @override
  List<Object> get props => [error];
}