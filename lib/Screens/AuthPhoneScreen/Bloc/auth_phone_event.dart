part of 'auth_phone_bloc.dart';

abstract class AuthPhoneEvent extends Equatable {
  const AuthPhoneEvent();
}

class SendPhoneNumber extends AuthPhoneEvent {
  final String phoneNumber;
  final String deviceId;

  const SendPhoneNumber({this.phoneNumber, this.deviceId});

  @override
  // TODO: implement props
  List<Object> get props => [phoneNumber];
}

class InitialLoad extends AuthPhoneEvent {
  const InitialLoad();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class SetError extends AuthPhoneEvent {
  final String error;

  const SetError(this.error);

  @override
  // TODO: implement props
  List<Object> get props => [error];
}
