import 'dart:convert';

import 'package:faem_drink/Screens/SettingsScreen/view/settings_screen.dart';
import 'package:faem_drink/Screens/SettingsScreen/view/settings_screen_view.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/models/main_user.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:faem_drink/data/data.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HistoryScreen extends StatefulWidget {
  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  final AuthenticationManager authenticationManager =
      locator.get<AuthenticationManager>();
  MainUser mainUser;

  _setUserName() async {
    final mainUserString = await locator.get<SecureStorage>().getData('MainUser');
    mainUser = MainUser.fromJson(json.decode(mainUserString));
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _setUserName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50.0),
          ),
          child: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_rounded,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              mainUser.customer.firstname,
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
            Text(
              mainUser.customer.phone,
              style: TextStyle(color: Colors.black, fontSize: 15),
            ),
          ],
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.settings,
              color: AppColor.textColor,
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsScreen()));
            },
          ),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.access_time_sharp,
              size: 80.0,
            ),
            Text(
              'Здесь будет ваша \nистория заказов',
              style: TextStyle(fontSize: 18),
              textAlign: TextAlign.center,
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 5.0),
                  child: Text(
                    'cделать заказ',
                    style:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.normal),
                  ),
                ),
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
