import 'dart:html';

import 'package:faem_drink/Screens/VerificationScreen/Provider/verification_api_provider.dart';
import 'package:faem_drink/Screens/VerificationScreen/Provider/verification_db_provider.dart';
import 'package:faem_drink/core/services/firebase.dart';
import 'package:faem_drink/core/services/network_info.dart';
import 'package:flutter/material.dart';

abstract class SettingsRepository<T> {
  Future<T> changeUserData({String name});
  Future<T> logout();
}

class SettingsRepositoryImpl implements SettingsRepository {

  final VerificationApiProvider apiProvider;
  final VerificationDBProvider dbProvider;
  final NetworkInfo networkInfo;
  final FirebaseService firebaseService;

  SettingsRepositoryImpl({
    @required this.apiProvider,
    @required this.dbProvider,
    @required this.networkInfo,
    @required this.firebaseService,
  })  : assert(apiProvider != null),
        assert(dbProvider != null),
        assert(networkInfo != null),
        assert(firebaseService != null);

  @override
  Future changeUserData({String name}) {
    dbProvider.storeUser(
      name: name,
    );
  }

  @override
  Future logout() {
    // TODO: implement logout
    dbProvider.removeUser();
  }

}

