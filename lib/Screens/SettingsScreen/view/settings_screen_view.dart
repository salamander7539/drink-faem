import 'package:faem_drink/Screens/HomeScreen/View/home_screen.dart';
import 'package:faem_drink/Screens/SettingsScreen/bloc/settings_bloc.dart';
import 'package:faem_drink/Screens/SettingsScreen/widgets/buttons.dart';
import 'package:faem_drink/Screens/SettingsScreen/widgets/text_field.dart';
import 'package:faem_drink/data/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsScreenView extends StatelessWidget {
  const SettingsScreenView({Key key, this.context}) : super(key: key);
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.black,
          ), onPressed: () {
            Navigator.of(this.context).pop();
        },
        ),
        title: Text(
          'Профиль',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: Text(
              'СОХРАНИТЬ',
            ),
          ),
        ],
        elevation: 0.5,
      ),
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          children: [
            SettingsTextFields(),
            SettingsTextFields(),
            SettingsTextFields(),
            Divider(),
            SettingsButton(buttonText: 'О приложение', icon: Icon(Icons.chevron_right),),
            SettingsButton(buttonText: 'Обратная связь ', icon: Icon(Icons.mail),),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: InkWell(
                onTap: () {
                  BlocProvider.of<SettingsBlocImpl>(context).add(Logout());
                  Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context) => HomeScreen()), (route) => false);
                },
                child: Container(
                  height: 65.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(36.0)),
                    color: Color(0xFFF2F3F5),
                  ),
                  child: Center(
                    child: Text(
                      'Выйти',
                      style: TextStyle(color: Colors.red, fontSize: 18),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
