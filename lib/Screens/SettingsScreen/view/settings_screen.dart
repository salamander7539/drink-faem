import 'package:faem_drink/Screens/SettingsScreen/bloc/settings_bloc.dart';
import 'package:faem_drink/Screens/SettingsScreen/view/settings_screen_view.dart';
import 'package:faem_drink/Screens/VerificationScreen/injections/verification_repository_di.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsScreen extends StatelessWidget {
  const SettingsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<SettingsBlocImpl>(
      create:  (context) => SettingsBlocImpl(
        repository: VerificationRepositoryInject.verificationRepository(),
        authenticationManager: locator.get<AuthenticationManager>(),
      ),
      child: SettingsScreenView(context: context,),
    );
  }
}
