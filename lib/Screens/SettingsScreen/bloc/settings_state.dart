part of 'settings_bloc.dart';

abstract class SettingsState extends Equatable {
  const SettingsState();

  @override
  List<Object> get props => [];
}

class SettingsInitialState extends SettingsState {}

class SettingsChangedState extends SettingsState {
  final String name;


  const SettingsChangedState({this.name, });

  @override
  List<Object> get props => [name,];
}

class LogoutState extends SettingsState {}


