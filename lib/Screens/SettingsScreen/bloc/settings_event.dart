part of 'settings_bloc.dart';

abstract class SettingsEvent extends Equatable {
  const SettingsEvent();
}

class ChangeUserData extends SettingsEvent {
  final String name;


  const ChangeUserData({this.name,});

  @override
  List<Object> get props => [name,];
}

class Logout extends SettingsEvent {
  const Logout();

  @override
  List<Object> get props => [];
}