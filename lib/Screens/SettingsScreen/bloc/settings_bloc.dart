import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:faem_drink/Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'package:faem_drink/Screens/VerificationScreen/repositories/verification_repository.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:flutter/material.dart';

part 'settings_event.dart';

part 'settings_state.dart';

abstract class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  SettingsBloc(SettingsState state) : super(state);
}

class SettingsBlocImpl extends Bloc<SettingsEvent, SettingsState> {
  final VerificationRepository repository;
  final AuthenticationManager authenticationManager;

  SettingsBlocImpl({this.repository, this.authenticationManager})
      : assert(repository != null),
        assert(authenticationManager != null),
        super(SettingsInitialState());

  @override
  Stream<SettingsState> mapEventToState(SettingsEvent event) async* {
    if (event is ChangeUserData) {
      yield SettingsChangedState();
    }
    if (event is Logout) {
      print("repository.logout");
      repository.logout();
      if (authenticationManager != null) {
        authenticationManager.deactivate();
        print("authenticationManager___ ${authenticationManager.isAuth}");
      }
    }
  }
}
