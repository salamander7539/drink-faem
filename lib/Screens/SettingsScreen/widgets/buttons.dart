import 'package:flutter/material.dart';

class SettingsButton extends StatelessWidget {
  String buttonText;
  Icon icon;
  SettingsButton({Key key, this.buttonText, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      child: InkWell(
        onTap: () {},
        child: Container(
          height: 55.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: Color(0xFFF2F3F5),
          ),
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(buttonText),
                icon,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
