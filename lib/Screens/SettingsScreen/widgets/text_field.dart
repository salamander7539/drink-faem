import 'package:faem_drink/data/data.dart';
import 'package:flutter/material.dart';

class SettingsTextFields extends StatefulWidget {
  const SettingsTextFields({Key key}) : super(key: key);

  @override
  _SettingsTextFieldsState createState() => _SettingsTextFieldsState();
}

class _SettingsTextFieldsState extends State<SettingsTextFields> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: TextFormField(
        decoration: InputDecoration(
          filled: true,
          fillColor: Color(0xFFF2F3F5),
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.circular(10.0)
          ),
        ),
      ),
    );
  }
}
