import 'package:faem_drink/Screens/VerificationScreen/Provider/verification_api_provider.dart';

class VerificationApiProviderInject {
  static VerificationApiProvider _verificationApiProvider;

  VerificationApiProviderInject._();

  static VerificationApiProvider verificationApiProvider() {
    if (_verificationApiProvider == null) {
      _verificationApiProvider = VerificationApiProviderImpl();
    }
    return _verificationApiProvider;
  }
}