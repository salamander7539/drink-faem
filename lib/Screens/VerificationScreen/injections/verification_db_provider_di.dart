import 'package:faem_drink/Screens/VerificationScreen/Provider/verification_db_provider.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/secure_storage.dart';

class VerificationDBProviderInject {
  static VerificationDBProvider _verificationDBProvider;

  VerificationDBProviderInject._();

  static VerificationDBProvider verificationDBProvider() {
    if (_verificationDBProvider == null) {
      _verificationDBProvider = VerificationDBProviderImpl(secureStorage: locator.get<SecureStorage>());
    }
    return _verificationDBProvider;
  }
}