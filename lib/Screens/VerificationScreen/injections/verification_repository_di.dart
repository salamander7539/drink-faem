import 'package:faem_drink/Screens/VerificationScreen/injections/verfication_api_provider_di.dart';
import 'package:faem_drink/Screens/VerificationScreen/injections/verification_db_provider_di.dart';
import 'package:faem_drink/Screens/VerificationScreen/repositories/verification_repository.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/firebase.dart';
import 'package:faem_drink/core/services/network_info.dart';

class VerificationRepositoryInject {
  static VerificationRepository _verificationRepository;

  VerificationRepositoryInject._();

  static VerificationRepository verificationRepository() {
    if (_verificationRepository == null) {
      _verificationRepository = VerificationRepositoryImpl(
          apiProvider: VerificationApiProviderInject.verificationApiProvider(),
          dbProvider: VerificationDBProviderInject.verificationDBProvider(),
          networkInfo: locator.get<NetworkInfo>(),
          firebaseService: locator.get<FirebaseService>()
      );
    }
    return _verificationRepository;
  }
}
