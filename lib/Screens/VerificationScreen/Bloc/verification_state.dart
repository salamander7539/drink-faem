part of 'verification_bloc.dart';

abstract class VerificationState extends Equatable {
  const VerificationState();
    @override
    List<Object> get props => [];
}

class VerificationInitialState extends VerificationState {}

class VerificationLoadingState extends VerificationState {}

class VerificationSuccessState extends VerificationState {
  final VerificationData authCodeData;
  final bool goToHomeScreen;
  final bool goToNameScreen;

  const VerificationSuccessState(this.authCodeData, {this.goToHomeScreen = false, this.goToNameScreen = false});

  @override
  List<Object> get props => [authCodeData, goToHomeScreen, goToNameScreen];
}

class VerificationErrorState extends VerificationState {
  final String error;

  const VerificationErrorState({this.error});

  @override
  List<Object> get props => [error];
}

