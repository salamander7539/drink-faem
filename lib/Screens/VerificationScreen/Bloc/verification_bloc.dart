import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:faem_drink/Config/config.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/UserName.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:faem_drink/Screens/VerificationScreen/repositories/verification_repository.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:faem_drink/data/data.dart';
import 'package:faem_drink/data/global_variables.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

part 'verification_event.dart';

part 'verification_state.dart';

abstract class VerificationBloc extends Bloc<VerificationEvent, VerificationState> {
  VerificationBloc(VerificationState state) : super(state);
}

class VerificationBlocImpl extends Bloc<VerificationEvent, VerificationState> {

  final VerificationRepository repository;
  final AuthenticationManager authenticationManager;
  final NecessaryDataForAuth necessaryDataForAuth;

  VerificationBlocImpl({this.repository, this.authenticationManager, this.necessaryDataForAuth})
      : assert(repository != null),
        assert(authenticationManager != null),
        super(VerificationInitialState());

  @override
  Stream<VerificationState> mapEventToState(VerificationEvent event) async* {
    if (event is SendCode) {

      yield VerificationLoadingState();
      try {
        // final String verData = await repository.fetchData(
        //   deviceId: 'DF7243D2-AB8C-4609-AC60-D8815202A0FE',
        // );

        final String verData = await repository.fetchData(deviceId: event.deviceId.toString(), code: event.code.toString(), phone: event.phone.toString());
        print("AuthData____1   $verData");
        print("check auth______ ${authenticationManager == null}");
        print ('eventCode___ ${event.code}');

        if (authenticationManager != null) authenticationManager.activate();
        print("AUTH ${authenticationManager.isAuth}");

      } on Exception catch (e) {
        yield VerificationErrorState(error: e.toString());
      }
    }

    if (event is VerificationCompleted) {
      try {
        final UserName userName = await repository.setClientName(name: event.name.toString(), uuid: event.userId.toString());
        print('userName______${userName.name}');
        if (userName.name != null) {
          authenticationManager.activate();
          print("AUTH ${authenticationManager.isAuth}");
        }
      } on Exception catch (e) {
        yield VerificationErrorState(error: e.toString());
      }
    }

    if (event is UserLogouted) {
      print("repository.logout");
      repository.logout();
      if (authenticationManager != null) authenticationManager.deactivate();
    }

    if (event is CodeButtonPressed) {}

    // Обновить токен
    if (event is TokenRefreshed)  {
      print("TokenRefreshed__1");
      await repository.refreshJwt();
      print("TokenRefreshed__2");
    }
  }
}

//   if (event is InitialLoad) {
//     yield AuthCodeInitialState();
//   } else if (event is SendCode) {
//     yield AuthCodeLoadingState();
//     if (event.code == null || event.code.toString() == '') {
//       yield AuthCodeErrorState('Вы ввели неверный смс код');
//       return;
//     }
//     // authCodeData = await loadAuthCodeData(necessaryDataForAuth.device_id, event.code);
//     if (authCodeData != null) {
//
//       necessaryDataForAuth.phone_number =
//           currentUser.phone;
//       necessaryDataForAuth.refresh_token =
//           authCodeData.refreshToken.value;
//       necessaryDataForAuth.token =
//           authCodeData.token;
//       await NecessaryDataForAuth.saveData();
//
//       currentUser.isLoggedIn = true;
//       if(necessaryDataForAuth.name == null) {
//         yield AuthCodeSuccessState(null, goToHomeScreen: false, goToNameScreen: true);
//       } else {
//         yield AuthCodeSuccessState(null, goToNameScreen: false, goToHomeScreen: true);
//       }
//     } else {
//       yield AuthCodeErrorState('Вы ввели неверный смс код');
//     }
//   } else if (event is CodeError) {
//     yield AuthCodeErrorState(event.error);
//   }
// }
