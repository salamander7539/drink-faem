part of 'verification_bloc.dart';

abstract class VerificationEvent extends Equatable {
  const VerificationEvent();
}

class InitialLoad extends VerificationEvent {
  const InitialLoad();

  @override
  List<Object> get props => [];
}

class SendCode extends VerificationEvent {
  final int code;
  final String deviceId;
  final String phone;


  const SendCode({@required this.code, @required this.deviceId, this.phone});


  @override
  List<Object> get props => [code, deviceId, phone];

}

class VerificationCompleted extends VerificationEvent {
  final String name;
  final String userId;

  const VerificationCompleted({
    @required this.name, @required this.userId
  });

  @override
  List<Object> get props => [name, userId];

  @override
  String toString() =>
      'LoginButtonPressed {phoneNumber: $name}';
}

class CodeError extends VerificationEvent {
  final String error;
  const CodeError({this.error});

  @override
  List<Object> get props => [error];
}

class UserLogouted extends VerificationEvent {
  const UserLogouted();
  @override
  List<Object> get props => [];
}


class CodeButtonPressed extends VerificationEvent {
  const CodeButtonPressed();
  @override
  List<Object> get props => [];
}


class TokenRefreshed extends VerificationEvent {
  const TokenRefreshed();
  @override
  List<Object> get props => [];
}