import 'dart:convert';

import 'package:faem_drink/Screens/VerificationScreen/Model/UserName.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:faem_drink/Screens/VerificationScreen/Provider/verification_api_provider.dart';
import 'package:faem_drink/Screens/VerificationScreen/Provider/verification_db_provider.dart';
import 'package:faem_drink/core/services/firebase.dart';
import 'package:faem_drink/core/services/network_info.dart';
import 'package:flutter/foundation.dart';

abstract class VerificationRepository<T> {
  Future<T> fetchData({String deviceId, String code, String name, String phone});

  Future<T> logout();

  Future<T> refreshJwt();

  Future<T> setClientName({String uuid, String name});
}

class VerificationRepositoryImpl implements VerificationRepository {
  final VerificationApiProvider apiProvider;
  final VerificationDBProvider dbProvider;
  final NetworkInfo networkInfo;
  final FirebaseService firebaseService;

  VerificationRepositoryImpl(
      {@required this.apiProvider, @required this.dbProvider, this.networkInfo, this.firebaseService})
      : assert(apiProvider != null),
        assert(dbProvider != null),
        assert(networkInfo != null),
        assert(firebaseService != null);

  @override
  logout() {
    // TODO: implement logout
    dbProvider.removeUser();
  }

  @override
  // ignore: missing_return
  Future<String> fetchData({String deviceId, String code, String name, String phone}) async {
    if (await networkInfo.isConnected) {
      final VerificationData data = await apiProvider.verification(deviceId: deviceId, code: code, phone: phone);
      print(data.toString());

      var fcmToken = await firebaseService.getFirebaseToken();

      var fcmTokenResp = await firebaseService.sendFCMToken(fcmToken: fcmToken, bearerToken: data.token, deviceId: deviceId);

      var centrifugoToken = await apiProvider.getCentrifugoToken(data.token);

      print("CentrifugoToken___ $centrifugoToken");
      if(fcmTokenResp) {
        dbProvider.storeUser(
          object: data,
          name: name,
          phone: phone,
          fcmToken: fcmToken,
          centrifugoToken: centrifugoToken,
        );
      }
      print("PHONE___${json.decode(dbProvider.toString())}");
    } else {
      print('Check network connection');
    }
  }



  @override
  // ignore: missing_return
  Future<String> refreshJwt() async {
    String refreshToken = await dbProvider.getRefreshToken();
    print("refreshToken__ $refreshToken");
    VerificationData verificationData = await apiProvider.refreshJwt(refreshToken);
    print("authData_!__ $verificationData");
    await dbProvider.storeUserWithNewToken(verificationData);
  }

  @override
  Future<UserName> setClientName({String uuid, String name}) async {
    if (await networkInfo.isConnected) {
      final UserName userName = await apiProvider.setClientName(uuid: uuid, name: name);

      if (userName != null) {
        dbProvider.storeUser(name: name, userID: uuid);
        return userName;
      } else {
        dbProvider.storeUser(name: name, userID: uuid);
        return userName;
      }
    }
  }
}
