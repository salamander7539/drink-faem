import 'dart:convert';

import 'package:faem_drink/core/models/main_user.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:flutter/foundation.dart';

abstract class VerificationDBProvider<T> {
  Future<String> getToken();

  Future<String> getRefreshToken();

  Future storeUser(
      {T object,
      String name,
      String phone,
      String userID,
      String fcmToken,
      String centrifugoToken});

  Future removeUser();

  Future storeUserWithNewToken(dynamic verificationData);
}

class VerificationDBProviderImpl implements VerificationDBProvider {
  final SecureStorage secureStorage;

  VerificationDBProviderImpl({@required this.secureStorage})
      : assert(secureStorage != null);

  @override
  Future<String> getToken() async {
    return await secureStorage.getData('Token');
  }

  @override
  Future<String> getRefreshToken() async {
    final mainUserString = await secureStorage.getData('MainUser');
    final MainUser mainUser = MainUser.fromJson(json.decode(mainUserString));
    return mainUser.verificationData.refreshToken.value;
  }

  @override
  Future storeUser({dynamic object, String name, String phone, String userID, String fcmToken, String centrifugoToken}) async {

    final row = await secureStorage.getData("MainUser");

    final prevUser = (row == null) ? null : MainUser.fromJson(json.decode(await secureStorage.getData('MainUser')));

    final MainUser mainUser = MainUser(
        centrifugotoken: centrifugoToken ?? prevUser.centrifugotoken,
        fcmToken: fcmToken ?? prevUser.fcmToken,
        verificationData: object ?? prevUser.verificationData,
        customer: Customer(
          firstname: name ?? prevUser?.customer?.firstname,
          phone: phone ?? prevUser?.customer?.phone,
          lastname: '' ?? prevUser?.customer?.lastname,
        )
    );

    secureStorage.putData('MainUser', json.encode(mainUser.toJson()));
  }

  @override
  Future removeUser() async {
    print("removeUser");
    secureStorage.removeData('MainUser');
  }

  @override
  Future storeUserWithNewToken(dynamic verificationData) async {
    print('storeUserWithNewToken__ ');
    final mainUserString = await secureStorage.getData('MainUser');
    final MainUser mainUser = MainUser.fromJson(json.decode(mainUserString));
    final mainUserUpdated = mainUser.copyWith(verificationData: verificationData);
    print("mainUserUpdated___ $mainUserUpdated");
    await secureStorage.putData('MainUser', json.encode(mainUserUpdated.toJson()));
  }
}
