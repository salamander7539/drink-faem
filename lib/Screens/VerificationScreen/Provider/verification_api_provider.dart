import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/UserName.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:faem_drink/core/global/contrains.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/models/main_user.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:faem_drink/data/global_variables.dart';

import 'dart:convert' as convert;
import 'package:http/http.dart' as http;


abstract class VerificationApiProvider<T> {
  Future<T> verification({String deviceId, String code, String phone});

  Future<T> getClosestRegion(String line);

  Future<T> refreshJwt(String refreshToken);

  Future<T> getCentrifugoToken(String bearerToken);

  Future<T> setClientName({String uuid, String name});
}

class VerificationApiProviderImpl implements VerificationApiProvider {
  final Dio _dio = Dio();

  @override
  // ignore: missing_return
  Future<VerificationData> verification(
      {String deviceId, String code, String phone}) async {
    int code1 = int.parse(code);

    try {
      print("device: $deviceId, code: $code1");
      Response response = await _dio.post(
        VERIFICATION_ENDPOINT,
        data: {'device_id': deviceId, 'code': code1},
        options: Options(headers: <String, String>{
          'Source': 'eda/faem'
        }),
      );
      print(response.data);
      print("statusCode from verification ${response.statusCode}");
      if (response.statusCode == 200) {
        print(response.data);
        return VerificationData.fromJson(response.data);
      } else {
        print('Verification failed with status: ${response.statusCode}.');
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  Future<UserName> setClientName({String uuid, String name}) async {
    UserName userName;
    final mainUserString = await locator.get<SecureStorage>().getData('MainUser');
    final MainUser mainUser = MainUser.fromJson(json.decode(mainUserString));
    var url = '${apiUrl}/clients/$uuid/$name';
    try {
      var response = await _dio.put(url, options: Options(headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization':'Bearer ' + mainUser.verificationData.token,
      }));
      if (response.statusCode == 200) {
        if(response.data != null){
          userName = new UserName.fromJson(response.data);
        }
      } else {
        print('Request failed with status: ${response.statusCode}.');
      }
      print(userName.name + '_____USER NAME');
      return userName;
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  @override
  // ignore: missing_return
  Future refreshJwt(String refreshToken) async {
    print('refreshJwt____$refreshToken)');
    Response response = await _dio.post(
      REFRESH_JWT_ENDPOINT,
      data: {
        'refresh': refreshToken,
      },
      options: Options(
        headers: {
          "Content-Type": "application/json",
        },
      ),
    );
    // print('refreshJwt response __ ${response.data}');
    if (response.statusCode == 200) {
      print(response.data);
      return VerificationData.fromJson(response.data);
    } else {
      print('Refresh failed with status: ${response.statusCode}.');
    }
  }

  @override
  // ignore: missing_return
  Future getClosestRegion(String line) async {}

  @override
  // ignore: missing_return
  Future getCentrifugoToken(String bearerToken) async {
    var headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $bearerToken'
    };

    var url = 'https://notifier.apis.stage.faem.pro/api/v2/connectiontoken';

    var response = await http.get(Uri.parse(url), headers: headers);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      print('jsonResponse: $jsonResponse');
      return jsonResponse['token'];
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }
}

//
// Future<AuthCodeData> loadAuthCodeData(String device_id, int code) async {
//   final Dio _dio = Dio();
//
//   AuthCodeData authCodeData = null;
//   var json_request = {
//     "device_id": device_id,
//     "code": code,
//   };
//   var url = '${authApiUrl}clients/verification';
//   try {
//     Response response = await _dio.post(VERIFICATION_ENDPOINT,
//         data: json_request,
//         options: Options(headers: {
//           'Content-Type': 'application/json; charset=UTF-8',
//           "Source": "eda/faem"
//         }));
//     if (response.statusCode == 200) {
//       var jsonResponse = convert.jsonDecode(response.data);
//       authCodeData = new AuthCodeData.fromJson(jsonResponse);
//     } else {
//       print('Request failed with status: ${response.statusCode}.');
//     }
//   } on DioError catch(e) {
//     print(e.response.data['message']);
//   }
//
//   return authCodeData;
// }
