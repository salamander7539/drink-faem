// To parse this JSON data, do
//
//     final userName = userNameFromJson(jsonString);

import 'dart:convert';

UserName userNameFromJson(String str) => UserName.fromJson(json.decode(str));

String userNameToJson(UserName data) => json.encode(data.toJson());

class UserName {
  UserName({
    this.uuid,
    this.name,
    this.application,
    this.mainPhone,
    this.devicesId,
    this.blocked,
    this.addresses,
    this.meta,
  });

  String uuid;
  String name;
  String application;
  String mainPhone;
  List<String> devicesId;
  bool blocked;
  dynamic addresses;
  Meta meta;

  factory UserName.fromJson(Map<String, dynamic> json) => UserName(
    uuid: json["uuid"] == null ? null : json["uuid"],
    name: json["name"] == null ? null : json["name"],
    application: json["application"] == null ? null : json["application"],
    mainPhone: json["main_phone"] == null ? null : json["main_phone"],
    devicesId: json["devices_id"] == null ? null : List<String>.from(json["devices_id"].map((x) => x)),
    blocked: json["blocked"] == null ? null : json["blocked"],
    addresses: json["addresses"],
    meta: json["meta"] == null ? null : Meta.fromJson(json["meta"]),
  );

  Map<String, dynamic> toJson() => {
    "uuid": uuid == null ? null : uuid,
    "name": name == null ? null : name,
    "application": application == null ? null : application,
    "main_phone": mainPhone == null ? null : mainPhone,
    "devices_id": devicesId == null ? null : List<dynamic>.from(devicesId.map((x) => x)),
    "blocked": blocked == null ? null : blocked,
    "addresses": addresses,
    "meta": meta == null ? null : meta.toJson(),
  };
}

class Meta {
  Meta();

  factory Meta.fromJson(Map<String, dynamic> json) => Meta(
  );

  Map<String, dynamic> toJson() => {
  };
}
