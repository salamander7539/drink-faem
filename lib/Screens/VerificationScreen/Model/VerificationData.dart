import 'package:jwt_decoder/jwt_decoder.dart';

class VerificationData {
  VerificationData({
    this.token,
    this.refreshToken,
    this.userUuid
  });

  String token;
  RefreshToken refreshToken;
  String userUuid;


  factory VerificationData.fromJson(Map<String, dynamic> json) => VerificationData(
      token: json["token"] == null ? null : json["token"],
      refreshToken: json["refresh_token"] == null ? null : RefreshToken.fromJson(json["refresh_token"]),
      userUuid: json["token"] == null ? null : JwtDecoder.decode(json["token"])['uuid']
  );

  Map<String, dynamic> toJson() => {
    "token": token == null ? null : token,
    "refresh_token": refreshToken == null ? null : refreshToken.toJson(),
  };
}

class RefreshToken {
  RefreshToken({
    this.value,
    this.createdAt,
    this.expired,
  });

  String value;
  DateTime createdAt;
  int expired;

  factory RefreshToken.fromJson(Map<String, dynamic> json) => RefreshToken(
    value: json["value"] == null ? null : json["value"],
    createdAt: json["created_at"] == null ? null : DateTime.parse(json["created_at"]),
    expired: json["expired"] == null ? null : json["expired"],
  );

  Map<String, dynamic> toJson() => {
    "value": value == null ? null : value,
    "created_at": createdAt == null ? null : createdAt.toIso8601String(),
    "expired": expired == null ? null : expired,
  };
}

