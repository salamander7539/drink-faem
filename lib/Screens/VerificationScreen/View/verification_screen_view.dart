import 'package:device_info/device_info.dart';
import 'package:faem_drink/Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'package:faem_drink/Screens/VerificationScreen/Widgets/send_another_code.dart';
import 'package:faem_drink/Screens/HomeScreen/View/home_screen.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:faem_drink/data/data.dart';
import 'package:faem_drink/data/global_variables.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

import 'input_name.dart';

class VerificationScreenView extends StatefulWidget {
  String phone;


  VerificationScreenView({this.phone});
  @override
  _VerificationScreenViewState createState() => _VerificationScreenViewState();
}

class _VerificationScreenViewState extends State<VerificationScreenView> {
  TextEditingController pinController = new TextEditingController();
  var devId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getId();
  }

  void getId() async {
    devId = await DeviceInfoPlugin().androidInfo.then((value) => value.id);

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        leading: Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: IconButton(
            icon: SvgPicture.asset('assets/svg_images/arrow_back.svg'),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        title: Text(
          'SMS-код',
          style: TextStyle(
            color: AppColor.textColor,
          ),
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 130.0),
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(
                          text: 'Код отправили на номер',
                        ),
                        new TextSpan(
                          text: '\n${widget.phone}',
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 70),
                  child: PinInputTextField(
                    autoFocus: true,
                    pinLength: 4,
                    decoration: BoxLooseDecoration(
                      bgColorBuilder: PinListenColorBuilder(
                        Color(0xFFEAEAEA),
                        Color(0xFFEAEAEA),
                      ),
                      strokeColorBuilder: PinListenColorBuilder(
                          Colors.transparent, Colors.transparent),
                    ),
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly,
                    ],
                    onChanged: (value) {
                      if (value.length == 4) {
                        pinController.text = value;
                        print(pinController.text);
                        BlocProvider.of<VerificationBlocImpl>(context).add(SendCode(code: int.parse(pinController.text), deviceId: devId, phone: widget.phone));
                        Navigator.push(context, MaterialPageRoute(builder: (context) => InputName()));
                      }
                    },
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        right: 16.0, left: 16.0, bottom: 90),
                    child: SendAnotherCodeButton(),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
