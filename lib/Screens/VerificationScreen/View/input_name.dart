import 'package:faem_drink/Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'package:faem_drink/Screens/VerificationScreen/injections/verification_repository_di.dart';
import 'package:faem_drink/Screens/VerificationScreen/repositories/verification_repository.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'input_name_view.dart';

class InputName extends StatelessWidget {
  String name;
  String userId;

  InputName({this.name, this.userId});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VerificationBlocImpl>(
      create: (context) => VerificationBlocImpl(
        repository: VerificationRepositoryInject.verificationRepository(),
        authenticationManager: locator.get<AuthenticationManager>(),
      ),
      child: InputNameScreenView(name: name, userId: userId),
    );
  }
}
