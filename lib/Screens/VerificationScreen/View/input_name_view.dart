import 'dart:convert';

import 'package:faem_drink/Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'package:faem_drink/core/global/keys.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/models/main_user.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

class InputNameScreenView extends StatefulWidget {
  String name;
  String userId;

  InputNameScreenView({this.name, this.userId});

  @override
  _InputNameScreenViewState createState() => _InputNameScreenViewState();
}

class _InputNameScreenViewState extends State<InputNameScreenView> {
  final String hintText1 = "Нажимая кнопку “Далее”, вы принимете условия";
  final String hintText2 =
      "Пользовательского соглашения и Политики конфиденцальности";
  final String hintText = 'Ваше имя';
  final String errormassege = "Minimum is 1 characters";

  bool isActive = false;

  final GlobalKey<FormState> formkey = GlobalKey<FormState>();
  final nameController = TextEditingController();

  _clickButton() async {
    String name = nameController.text;
    // BlocProvider.of<LoginBlocImpl>(context).add(LoginCompleted(code: widget.code, name: name));
    final mainUserString = await locator.get<SecureStorage>().getData('MainUser');
    final MainUser mainUser = mainUserString == null ? null : MainUser.fromJson(json.decode(mainUserString));
    print('BLOC____${mainUser.verificationData.userUuid}');
    BlocProvider.of<VerificationBlocImpl>(context).add(VerificationCompleted(name: name, userId: mainUser.verificationData.userUuid));
    Keys.globalNavigationKey.currentState.pushNamed("home");
  }

  String _phoneValidate(String value) {
    if (value.isEmpty) {
      return errormassege;
    } else {
      setState(() {
        isActive = true;
      });
      return null;
    }
  }

  _validate(String value) {
    if (formkey.currentState.validate()) {
      print("validate");
    } else {
      print("not validate");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Scaffold(
        body: Column(
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Row(
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Padding(
                        padding: EdgeInsets.only(left: 0, top: 30),
                        child: Container(
                            height: 40,
                            width: 60,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 12, bottom: 12, right: 10),
                              child: SvgPicture.asset('assets/icons/Arrow.svg'),
                            ))),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(top: 35, bottom: 45),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 25),
                          child: Text("Как вас зовут?"),
                        ),
                        Card(
                          margin: EdgeInsets.only(left: 45, right: 45),
                          shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          child: Column(
                            children: [
                              Form(
                                key: formkey,
                                child: TextFormField(
                                  key: Key("j"),
                                  validator: _phoneValidate,
                                  controller: nameController,
                                  // inputFormatters: [formatter],
                                  style: TextStyle(fontSize: 18),
                                  onChanged: _validate,
                                  // onChanged:onChangedText,
                                  textAlign: TextAlign.left,
                                  autofocus: true,
                                  cursorColor: Colors.black,
                                  keyboardType: TextInputType.text,
                                  decoration: InputDecoration(
                                      errorStyle: TextStyle(color: Colors.red),
                                      border: InputBorder.none,
                                      focusedBorder: InputBorder.none,
                                      enabledBorder: InputBorder.none,
                                      errorBorder: InputBorder.none,
                                      disabledBorder: InputBorder.none,
                                      contentPadding:
                                          EdgeInsets.only(left: 20, right: 20),
                                      hintText: hintText),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        InkWell(
                          child: Container(
                            width: double.infinity,
                            height: 44,
                            decoration: BoxDecoration(
                              color: Color(0xFFF2F6F9),
                              borderRadius: BorderRadius.circular(32),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Далее',
                                  style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.normal,
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ],
                            ),
                          ),
                          onTap: () async {
                            _clickButton();
                            // Navigator.push(context, MaterialPageRoute(builder: (context) => AuthCodeScreen()));
                          },
                        ),
                        Container(
                            margin: EdgeInsets.only(
                              top: 15,
                            ),
                            // width: 320,
                            child: Text(hintText1,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.grey,
                                ))),
                        Container(
                          padding: EdgeInsets.only(left: 15, right: 15),
                          margin: EdgeInsets.only(bottom: 15),
                          // width: 320,
                          child: Text(
                            hintText2,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.grey,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
