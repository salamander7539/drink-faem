import 'package:faem_drink/Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'package:faem_drink/Screens/VerificationScreen/View/verification_screen.dart';
import 'package:faem_drink/Screens/VerificationScreen/View/verification_screen_view.dart';
import 'package:faem_drink/Screens/VerificationScreen/injections/verification_repository_di.dart';
import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/authentication_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VerificationScreen extends StatelessWidget {
  String phone;

  VerificationScreen({this.phone});

  @override
  Widget build(BuildContext context) {
    return BlocProvider<VerificationBlocImpl>(
      create:  (context) => VerificationBlocImpl(
        repository: VerificationRepositoryInject.verificationRepository(),
        authenticationManager: locator.get<AuthenticationManager>(),
      ),
      child: VerificationScreenView(phone: phone,),
    );
  }
}
