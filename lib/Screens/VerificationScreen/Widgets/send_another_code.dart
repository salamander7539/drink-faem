import 'package:device_info/device_info.dart';
import 'package:faem_drink/Screens/AuthPhoneScreen/Bloc/auth_phone_bloc.dart';
import 'package:faem_drink/data/data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SendAnotherCodeButton extends StatefulWidget {
  @override
  _SendAnotherCodeButtonState createState() => _SendAnotherCodeButtonState();
}

class _SendAnotherCodeButtonState extends State<SendAnotherCodeButton> {
  var devId;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    devId();
  }

  void getId() async {
    devId = await DeviceInfoPlugin().androidInfo.then((value) => value.id);
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        width: double.infinity,
        height: 44,
        decoration: BoxDecoration(
          color: Colors.grey.withOpacity(0.2),
          borderRadius: BorderRadius.circular(32),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'получить код снова',
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.normal,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      onTap: () {
        BlocProvider.of<AuthPhoneBlocImpl>(context).add(SendPhoneNumber(phoneNumber: currentUser.phone, deviceId: devId));
      },
    );
  }
}
