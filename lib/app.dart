import 'package:faem_drink/core/locator.dart';
import 'package:faem_drink/core/services/secure_storage.dart';
import 'package:faem_drink/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

import 'Screens/HomeScreen/view/home_screen.dart';
import 'Screens/VerificationScreen/Bloc/verification_bloc.dart';
import 'Screens/VerificationScreen/injections/verification_repository_di.dart';
import 'core/services/authentication_manager.dart';
import 'core/services/network_info.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    locator.get<AuthenticationManager>().init();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    locator.get<SecureStorage>().close();
    locator.reset(dispose: true);
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<VerificationBlocImpl>(create: (context) => VerificationBlocImpl(
            repository: VerificationRepositoryInject.verificationRepository(),
            authenticationManager: locator.get<AuthenticationManager>())),
        BlocProvider(create: (_) => locator.get<NetworkInfo>()),
        BlocProvider(create: (_) => locator.get<AuthenticationManager>()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
            child: child,
          );
        },
        theme: ThemeData(
          fontFamily: 'Roboto',
        ),
        title: 'FaemDrink',
        home: SplashScreen(),
      ),
    );
  }
}
