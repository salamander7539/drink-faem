import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:faem_drink/core/global/contrains.dart';
import 'package:faem_drink/data/global_variables.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert' as convert;


import '../data/data.dart';

class NecessaryDataForAuth{
  String device_id;
  String phone_number;
  String refresh_token;
  String name;
  String token;
 // FilteredCities city;
 //  PaymentMethod selectedPaymentMethod;
  bool vis;
  static NecessaryDataForAuth _necessaryDataForAuth;

  NecessaryDataForAuth({
    this.device_id,
    this.phone_number,
    this.token,
    //this.city,
    this.refresh_token,
    // this.selectedPaymentMethod,
    this.name,
    this.vis
  });

  static Future<NecessaryDataForAuth> getData() async{
    if(_necessaryDataForAuth != null)
      return _necessaryDataForAuth;
    String device_id = await DeviceInfoPlugin().androidInfo.then((value) => value.id);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phone_number = prefs.getString('phone_number');
    String refresh_token = prefs.getString('refresh_token');
    String name = prefs.getString('name');
    //String cityJson = prefs.getString('city');
    // String selectedPaymentMethodJSON = prefs.getString('selected_payment_method') ?? jsonEncode({
    //   "name": (Platform.isIOS) ? "ApplePay" : "GooglePay",
    //   "image": (Platform.isIOS) ? "assets/svg_images/apple_pay.svg"
    //       : "assets/svg_images/google_pay.svg",
    //   "tag": "virtualCardPayment",
    //   "outputTag": "card"
    // });
    String token = prefs.getString('token');
    bool vis = prefs.getBool('vis') ?? true;


    // FilteredCities city;
    // PaymentMethod selectedPaymentMethod;
    //
    // // if(cityJson != null){
    // //   city = FilteredCities.fromJson(convert.jsonDecode(cityJson));
    // // }
    //
    // if(selectedPaymentMethodJSON != null){
    //   selectedPaymentMethod = PaymentMethod.fromJson(convert.jsonDecode(selectedPaymentMethodJSON));
    // }
    //
    //  await getColorScheme(header);
    await getAppInfo();


    NecessaryDataForAuth result = new NecessaryDataForAuth(
        device_id: device_id,
        phone_number: phone_number,
        refresh_token: refresh_token,
        name: name,
        token: token,
        // selectedPaymentMethod: selectedPaymentMethod,
        //city: city,
        vis: vis
    );

    refresh_token = await refreshToken(refresh_token, token, device_id);

    result.refresh_token = refresh_token;
    _necessaryDataForAuth = result;
    if(refresh_token != null){
      result.token = verificationData.token;
      await saveData();
    }

    return result;
  }

  static Future saveData() async{
    String phone_number = _necessaryDataForAuth.phone_number;
    String refresh_token = _necessaryDataForAuth.refresh_token;
    String name = _necessaryDataForAuth.name;
    //String city = convert.jsonEncode(_necessaryDataForAuth.city.toJson());
    // String selectedPaymentMethod = convert.jsonEncode(_necessaryDataForAuth.selectedPaymentMethod.toJson());
    String token = _necessaryDataForAuth.token;
    bool vis = _necessaryDataForAuth.vis;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('phone_number', phone_number);
    prefs.setString('refresh_token',refresh_token);
    prefs.setString('name',name);
    //prefs.setString('city',city);
    // prefs.setString('selected_payment_method',selectedPaymentMethod);
    prefs.setString('token',token);
    prefs.setBool('vis', vis);
  }

  static Future<String> refreshToken(String refresh_token, String token, String device_id) async {
    String result = null;
    var url = '${authApiUrl}auth/clients/refresh';
    print(refresh_token);
    print('--------');
    print(token);
    if(refresh_token == null || token == null){
      return null;
    }
    var response = await dio.post(url, data: {
      "device_id": device_id,
      "service": "eda",
      "refresh": refresh_token
      },
       options: Options(headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':'Bearer ' + token
        }));
    print(response.data);
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.data);
      verificationData = VerificationData.fromJson(jsonResponse);
      result = verificationData.refreshToken.value;
    } else {
      print('Request failed with status SERV: ${response.statusCode}.');
    }
    return result;
  }

  static Future clear() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
    _necessaryDataForAuth = null;
    necessaryDataForAuth = null;
  }
}