import 'package:dio/dio.dart';
import 'package:faem_drink/VersionControl/Model/CurrentVersionModel.dart';
import 'package:faem_drink/core/global/contrains.dart';
import 'package:faem_drink/data/data.dart';
import 'dart:convert' as convert;
import 'package:faem_drink/data/global_variables.dart';


Future<CurrentVersionModel> getCurrentVersion() async {
  CurrentVersionModel currentVersion;
  var url = '${apiUrl}versions/last';
  var response = await dio.get(url, options: Options(headers: {
    'Content-Type': 'application/json; charset=UTF-8',
    'Accept': 'application/json',
  }));
  print("Version ${response.data}");
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.data);
    currentVersion = new CurrentVersionModel.fromJson(jsonResponse);
    CheckVersion.fromJson(jsonResponse);
  } else {
    print('Request failed with status: ${response.statusCode}.');
  }
  print(response.data);
  return currentVersion;
}