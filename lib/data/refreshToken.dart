import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:faem_drink/core/global/contrains.dart';

import 'dart:convert' as convert;
import '../Config/config.dart';
import 'data.dart';
import 'global_variables.dart';

class SendRefreshToken {
  static Future<bool> sendRefreshToken(
      {String refreshToken, String token, String device_id}) async {
    bool isSuccess = false;
    try {
      var url = '${authApiUrl}auth/clients/refresh';
      var response = await dio.post(url,
          data: {
            "refresh": (refreshToken != null)
                ? refreshToken
                : verificationData.refreshToken.value,
            "service": "eda",
            "device_id":
                (device_id != null) ? device_id : necessaryDataForAuth.device_id
          },
          options: Options(
            headers: {
              'Content-Type': 'application/json; charset=UTF-8',
              'Authorization':
                  'Bearer ' + ((token != null) ? token : verificationData.refreshToken.value)
            },
          ));
      print('ТУТ КЕФРЕЭ ' + verificationData.refreshToken.value);
      if (response.statusCode == 200) {
        isSuccess = true;
        var jsonResponse = convert.jsonDecode(response.data);
        verificationData = VerificationData.fromJson(jsonResponse);
        necessaryDataForAuth.refresh_token = verificationData.refreshToken.value;
        necessaryDataForAuth.token = verificationData.token;
        await NecessaryDataForAuth.saveData();
        print(response.data);
      } else {
        print('Request failed with status: ${response.statusCode}.');
      }
    } catch (e) {
      isSuccess = false;
    }
    return isSuccess;
  }
}
