import 'package:dio/dio.dart';
import 'package:faem_drink/Config/config.dart';
import 'package:faem_drink/Screens/VerificationScreen/Model/VerificationData.dart';
import 'package:faem_drink/Screens/HomeScreen/View/home_screen.dart';
import 'package:flutter/material.dart';

String header = 'eda/faem';

VerificationData verificationData = null;
NecessaryDataForAuth necessaryDataForAuth = new NecessaryDataForAuth(phone_number: null, refresh_token: null, device_id: null, name: null);
GlobalKey<HomeScreenState>homeScreenKey = new GlobalKey<HomeScreenState>(debugLabel: 'homeScreenKey');
Dio dio = new Dio();
String id;
